package com.hopster.hopster.commonComponents;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.hopster.hopster.R;

/**
 * Created by thinuwan on 8/12/18.
 */

public class SnackBars {

    public static void showInvalidSnackBar(Context context, ConstraintLayout parentLayout, String message){
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("close", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                })
                .setActionTextColor(context.getResources().getColor(android.R.color.holo_red_light ))
                .show();
    }

    public static void showRedSnackBar(Context context, ConstraintLayout parentLayout, String message){
        Snackbar snackbar;
        snackbar = Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("close", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                })
                .setActionTextColor(context.getResources().getColor(android.R.color.white ));
        snackbar.show();
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.darkRed));
    }

    public static void showSuccessSnackBar(Context context, ConstraintLayout parentLayout, String message){
        Snackbar snackbar;
        snackbar = Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("close", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                })
                .setActionTextColor(context.getResources().getColor(android.R.color.white ));
        snackbar.show();
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
    }
}
