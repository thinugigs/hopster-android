package com.hopster.hopster.commonComponents;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.hopster.hopster.R;

/**
 * Created by thinuwan on 8/12/18.
 */

public class Dialogs {

    static ProgressDialog dialog;

    public static void showLoading(Context context, String message){
        dialog = ProgressDialog.show(context, "",
               message, true);
        dialog.show();
    }

    public static void stopLoading(){
        dialog.cancel();
    }
}
