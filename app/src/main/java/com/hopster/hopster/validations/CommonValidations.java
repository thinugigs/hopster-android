package com.hopster.hopster.validations;

import android.text.TextUtils;
import android.util.Patterns;

/**
 * Created by thinuwan on 8/12/18.
 */

public class CommonValidations {

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static boolean isNullOrEmptyOrWhitespace(String value) {
        if (value.trim().length() > 0) {
            return false;
        }
        return true;
    }
}
