package com.hopster.hopster.rest.responseBodyObjects.resetEmail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by thinuwan on 8/12/18.
 */

public class UpdatePasswordResponseBody {

    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("user")
    @Expose
    public User user;

}
