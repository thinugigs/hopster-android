package com.hopster.hopster.rest;

import com.hopster.hopster.rest.requestBodyObjects.CreateAccountRequestBody;
import com.hopster.hopster.rest.requestBodyObjects.EmailVerificationRequestBody;
import com.hopster.hopster.rest.requestBodyObjects.LoginRequestBody;
import com.hopster.hopster.rest.requestBodyObjects.UpdatePasswordRequestBody;
import com.hopster.hopster.rest.responseBodyObjects.EmailVerification.EmailVerificationResponseBody;
import com.hopster.hopster.rest.responseBodyObjects.login.LoginResponseBody;
import com.hopster.hopster.rest.responseBodyObjects.register.CreateAccountResponseBody;
import com.hopster.hopster.rest.responseBodyObjects.resetEmail.UpdatePasswordResponseBody;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by thinuwan on 8/10/18.
 */

public interface ApiInterface {

    @POST("login")
    Call<LoginResponseBody> getLoginReponse(@Body LoginRequestBody body);

    @POST("createNewUser")
    Call<CreateAccountResponseBody> getCreateAccountReponse(@Body CreateAccountRequestBody body);

    @POST("isEmailExist")
    Call<EmailVerificationResponseBody> getEmailVerificationReponse(@Body EmailVerificationRequestBody body);

    @POST("resetPassword")
    Call<UpdatePasswordResponseBody> getUpdatePasswordReponse(@Body UpdatePasswordRequestBody body);

}
