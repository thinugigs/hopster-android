package com.hopster.hopster.rest.requestBodyObjects;

/**
 * Created by thinuwan on 8/12/18.
 */

public class UpdatePasswordRequestBody {
    final String email;
    final String password;

    public UpdatePasswordRequestBody(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
