package com.hopster.hopster.rest.requestBodyObjects;

/**
 * Created by thinuwan on 8/12/18.
 */

public class EmailVerificationRequestBody {
    final String email;

    public EmailVerificationRequestBody(String email) {
        this.email = email;
    }
}
