package com.hopster.hopster.rest.responseBodyObjects.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by thinuwan on 8/12/18.
 */

public class CreateAccountResponseBody {
    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("result")
    @Expose
    public Result result;
}
