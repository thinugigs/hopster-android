package com.hopster.hopster.rest.requestBodyObjects;

/**
 * Created by thinuwan on 8/12/18.
 */

public class CreateAccountRequestBody {
    final String firstName;
    final String lastName;
    final String email;
    final String contactNumber;
    final String nicNumber;
    final String password;
    final String gender;

    public CreateAccountRequestBody(String firstName, String lastName, String email, String contactNumber, String nicNumber, String password, String gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.contactNumber = contactNumber;
        this.nicNumber = nicNumber;
        this.password = password;
        this.gender = gender;
    }
}
