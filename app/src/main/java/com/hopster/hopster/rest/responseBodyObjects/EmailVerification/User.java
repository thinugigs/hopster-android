package com.hopster.hopster.rest.responseBodyObjects.EmailVerification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by thinuwan on 8/12/18.
 */

public class User {


    @SerializedName("userId")
    @Expose
    public Integer userId;
    @SerializedName("firstName")
    @Expose
    public String firstName;
    @SerializedName("lastName")
    @Expose
    public String lastName;
    @SerializedName("contactNumber")
    @Expose
    public String contactNumber;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("password")
    @Expose
    public String password;
    @SerializedName("nicNumber")
    @Expose
    public String nicNumber;
    @SerializedName("gender")
    @Expose
    public String gender;
    @SerializedName("firebaseToken")
    @Expose
    public String firebaseToken;

}
