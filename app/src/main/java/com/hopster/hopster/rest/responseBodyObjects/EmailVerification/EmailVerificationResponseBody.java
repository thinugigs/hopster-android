package com.hopster.hopster.rest.responseBodyObjects.EmailVerification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by thinuwan on 8/12/18.
 */

public class EmailVerificationResponseBody {
    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("user")
    @Expose
    public List<User> user = null;
}
