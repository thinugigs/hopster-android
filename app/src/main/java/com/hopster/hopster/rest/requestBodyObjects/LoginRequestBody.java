package com.hopster.hopster.rest.requestBodyObjects;

/**
 * Created by thinuwan on 8/12/18.
 */

public class LoginRequestBody {
    final String email;
    final String password;

    public LoginRequestBody(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
