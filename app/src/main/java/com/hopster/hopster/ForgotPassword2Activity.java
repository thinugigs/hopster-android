package com.hopster.hopster;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import static com.hopster.hopster.commonComponents.SnackBars.showInvalidSnackBar;
import static com.hopster.hopster.commonComponents.SnackBars.showRedSnackBar;
import static com.hopster.hopster.permissionCheck.commonPermissionCheck.checkNetworkStatus;
import static com.hopster.hopster.validations.CommonValidations.isNullOrEmptyOrWhitespace;

public class ForgotPassword2Activity extends AppCompatActivity {

    private EditText editText_passcode;
    private Button button_next;
    private ConstraintLayout constraintLayout_root;

    private String snackBarMessage = "";
    private String email = "";

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password2);

        context = this;

        email = getIntent().getStringExtra("email");

        constraintLayout_root = (ConstraintLayout) findViewById(R.id.activity_forgotpassword2_constraintLayout);
        editText_passcode = (EditText) findViewById(R.id.activity_forgotpassword2_edittext_passcode);
        button_next = (Button)findViewById(R.id.activity_forgotpassword2_button_next);

        button_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonClickNext();
            }
        });

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void buttonClickNext(){
        if(checkNetworkStatus(context)){
            if(isValidator()){
                next();
            }
            else{
                showInvalidSnackBar(context, constraintLayout_root, snackBarMessage);
            }
        }
        else{
            showRedSnackBar(context, constraintLayout_root, "Please check your internet connection.");
        }
    }

    private void next(){
        Intent i = new Intent(getBaseContext(), ForgotPassword3Activity.class);
        i.putExtra("email", email);
        startActivity(i);
    }

    private boolean isValidator(){

        String email = editText_passcode.getText().toString();
        if(isNullOrEmptyOrWhitespace(email)){
            snackBarMessage = "Enter a valid passcode.";
            return false;
        }
        return true;
    }


}
