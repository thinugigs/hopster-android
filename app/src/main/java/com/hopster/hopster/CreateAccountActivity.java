package com.hopster.hopster;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.hopster.hopster.rest.ApiClient;
import com.hopster.hopster.rest.ApiInterface;
import com.hopster.hopster.rest.requestBodyObjects.CreateAccountRequestBody;
import com.hopster.hopster.rest.requestBodyObjects.LoginRequestBody;
import com.hopster.hopster.rest.responseBodyObjects.register.CreateAccountResponseBody;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.hopster.hopster.commonComponents.Dialogs.showLoading;
import static com.hopster.hopster.commonComponents.Dialogs.stopLoading;
import static com.hopster.hopster.permissionCheck.commonPermissionCheck.checkNetworkStatus;
import static com.hopster.hopster.commonComponents.SnackBars.showInvalidSnackBar;
import static com.hopster.hopster.commonComponents.SnackBars.showRedSnackBar;
import static com.hopster.hopster.validations.CommonValidations.isNullOrEmptyOrWhitespace;
import static com.hopster.hopster.validations.CommonValidations.isValidEmail;

public class CreateAccountActivity extends AppCompatActivity {

    private EditText editText_firstname, editText_lastname, editText_email, editText_contactnumber, editText_nic_passport, editText_password, editText_confirmpassword;
    private Button button_create_account;
    private ConstraintLayout constraintLayout_root;
    private RadioGroup radioGroup_gender;

    private String snackBarMessage = "";

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        context = this;

        constraintLayout_root = (ConstraintLayout) findViewById(R.id.activity_create_account_constraintLayout);
        editText_firstname = (EditText) findViewById(R.id.activity_create_account_edittext_firstname);
        editText_lastname = (EditText) findViewById(R.id.activity_create_account_edittext_lastname);
        editText_email = (EditText) findViewById(R.id.activity_create_account_edittext_email);
        editText_contactnumber = (EditText) findViewById(R.id.activity_create_account_edittext_contactnumber);
        editText_nic_passport = (EditText) findViewById(R.id.activity_create_account_edittext_nic_passport);
        editText_password = (EditText)findViewById(R.id.activity_create_account_edittext_password);
        editText_confirmpassword = (EditText) findViewById(R.id.activity_create_account_edittext_confirmpassword);
        button_create_account = (Button)findViewById(R.id.activity_create_account_button_create_account);
        radioGroup_gender = (RadioGroup)findViewById(R.id.activity_create_account_radiogroup_gender);

        button_create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonCreate();
            }
        });

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void buttonCreate(){
        if(checkNetworkStatus(context)){
            if(isValidator()){
                createAccount();
            }
            else{
                showInvalidSnackBar(context, constraintLayout_root, snackBarMessage);
            }
        }
        else{
            showRedSnackBar(context, constraintLayout_root, "Please check your internet connection.");
        }
    }

    private void createAccount(){

        showLoading(context, "Creating Account...");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        LoginRequestBody loginRequestBody = new LoginRequestBody(
                editText_email.getText().toString(),
                editText_password.getText().toString());

        int selectedGenderId =radioGroup_gender.getCheckedRadioButtonId();
        RadioButton selectedRadioButton = findViewById(selectedGenderId);

        CreateAccountRequestBody createAccountRequestBody = new CreateAccountRequestBody(
                editText_firstname.getText().toString(),
                editText_lastname.getText().toString(),
                editText_email.getText().toString(),
                editText_contactnumber.getText().toString(),
                editText_nic_passport.getText().toString(),
                editText_password.getText().toString(),
                selectedRadioButton.getText().toString()
        );

        Call<CreateAccountResponseBody> call = apiService.getCreateAccountReponse(createAccountRequestBody);
        call.enqueue(new Callback<CreateAccountResponseBody>() {
            @Override
            public void onResponse(Call<CreateAccountResponseBody> call, Response<CreateAccountResponseBody> response) {
                stopLoading();
                createAccountResultAction(response.body());
            }

            @Override
            public void onFailure(Call<CreateAccountResponseBody> call, Throwable t) {
                stopLoading();
                showInvalidSnackBar(context, constraintLayout_root, "Oops! Something went wrong.");
            }
        });
    }

    private void createAccountResultAction(CreateAccountResponseBody body){
        if (!body.status){
            showInvalidSnackBar(context, constraintLayout_root, "Oops! Something went wrong..");
        }
        else{
            Intent i = new Intent(getBaseContext(), LoginActivity.class);
            i.putExtra("message", "accountCreatedSuccessfully");
            startActivity(i);
        }
    }


    private boolean isValidator(){

        String firstname = editText_firstname.getText().toString();
        if(isNullOrEmptyOrWhitespace(firstname)){
            snackBarMessage = "Enter a valid firstname.";
            return false;
        }

        String lastname = editText_lastname.getText().toString();
        if(isNullOrEmptyOrWhitespace(lastname)){
            snackBarMessage = "Enter a valid lastname.";
            return false;
        }

        String email = editText_email.getText().toString();
        if(isNullOrEmptyOrWhitespace(email) || !isValidEmail(email)){
            snackBarMessage = "Enter a valid email.";
            return false;
        }

        String contactNumber = editText_contactnumber.getText().toString();
        if(isNullOrEmptyOrWhitespace(contactNumber)){
            snackBarMessage = "Enter a valid contactNumber.";
            return false;
        }


        String nic_passport = editText_nic_passport.getText().toString();
        if(isNullOrEmptyOrWhitespace(nic_passport)){
            snackBarMessage = "Enter a valid nic/passport.";
            return false;
        }

        String password = editText_password.getText().toString();
        if(isNullOrEmptyOrWhitespace(password)){
            snackBarMessage = "Enter a valid password.";
            return false;
        }

        String confirm_password = editText_confirmpassword.getText().toString();
        if(isNullOrEmptyOrWhitespace(confirm_password)){
            snackBarMessage = "Enter a valid confirm password.";
            return false;
        }

        if(!password.equals(confirm_password)){
            snackBarMessage = "Confirm password is not equal to password";
            return false;
        }

        if(radioGroup_gender.getCheckedRadioButtonId() == -1){
            snackBarMessage = "Select your gender.";
            return false;
        }
        return true;
    }

}
