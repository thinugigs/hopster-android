package com.hopster.hopster;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.hopster.hopster.rest.ApiClient;
import com.hopster.hopster.rest.ApiInterface;
import com.hopster.hopster.rest.requestBodyObjects.EmailVerificationRequestBody;
import com.hopster.hopster.rest.responseBodyObjects.EmailVerification.EmailVerificationResponseBody;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.hopster.hopster.commonComponents.Dialogs.showLoading;
import static com.hopster.hopster.commonComponents.Dialogs.stopLoading;
import static com.hopster.hopster.commonComponents.SnackBars.showInvalidSnackBar;
import static com.hopster.hopster.commonComponents.SnackBars.showRedSnackBar;
import static com.hopster.hopster.permissionCheck.commonPermissionCheck.checkNetworkStatus;
import static com.hopster.hopster.validations.CommonValidations.isNullOrEmptyOrWhitespace;
import static com.hopster.hopster.validations.CommonValidations.isValidEmail;

public class ForgotPassword1Activity extends AppCompatActivity {

    private EditText editText_email;
    private Button button_send;
    private ConstraintLayout constraintLayout_root;

    private String snackBarMessage = "";

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password1);

        context = this;

        constraintLayout_root = (ConstraintLayout) findViewById(R.id.activity_forgotpassword1_constraintLayout);
        editText_email = (EditText) findViewById(R.id.activity_forgotpassword1_edittext_email);
        button_send = (Button)findViewById(R.id.activity_forgotpassword1_button_send);

        button_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonClickSend();
            }
        });

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void buttonClickSend(){
        if(checkNetworkStatus(context)){
            if(isValidator()){
                send();
            }
            else{
                showInvalidSnackBar(context, constraintLayout_root, snackBarMessage);
            }
        }
        else{
            showRedSnackBar(context, constraintLayout_root, "Please check your internet connection.");
        }
    }

    private void send(){

        showLoading(context, "Checking email...");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        EmailVerificationRequestBody emailVerificationRequestBody =new EmailVerificationRequestBody(
                editText_email.getText().toString()
        );

        Call<EmailVerificationResponseBody> call = apiService.getEmailVerificationReponse(emailVerificationRequestBody);
        call.enqueue(new Callback<EmailVerificationResponseBody>() {
            @Override
            public void onResponse(Call<EmailVerificationResponseBody> call, Response<EmailVerificationResponseBody> response) {
                stopLoading();
                sendResultAction(response.body());
            }

            @Override
            public void onFailure(Call<EmailVerificationResponseBody> call, Throwable t) {
                stopLoading();
                showInvalidSnackBar(context, constraintLayout_root, "Oops! Something went wrong.");
            }
        });
    }

    private void sendResultAction(EmailVerificationResponseBody body){
        if (!body.status){
            showInvalidSnackBar(context, constraintLayout_root, "Email does not exists. Try again");
        }
        else{
            Intent i = new Intent(getBaseContext(), ForgotPassword2Activity.class);
            i.putExtra("email", editText_email.getText().toString());
            startActivity(i);
        }
    }


    private boolean isValidator(){

        String email = editText_email.getText().toString();
        if(isNullOrEmptyOrWhitespace(email) || !isValidEmail(email)){
            snackBarMessage = "Enter a valid email.";
            return false;
        }
        return true;
    }


}
