package com.hopster.hopster;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.hopster.hopster.rest.ApiClient;
import com.hopster.hopster.rest.ApiInterface;
import com.hopster.hopster.rest.requestBodyObjects.UpdatePasswordRequestBody;
import com.hopster.hopster.rest.responseBodyObjects.resetEmail.UpdatePasswordResponseBody;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.hopster.hopster.commonComponents.Dialogs.showLoading;
import static com.hopster.hopster.commonComponents.Dialogs.stopLoading;
import static com.hopster.hopster.commonComponents.SnackBars.showInvalidSnackBar;
import static com.hopster.hopster.commonComponents.SnackBars.showRedSnackBar;
import static com.hopster.hopster.permissionCheck.commonPermissionCheck.checkNetworkStatus;
import static com.hopster.hopster.validations.CommonValidations.isNullOrEmptyOrWhitespace;

public class ForgotPassword3Activity extends AppCompatActivity {

    private EditText editText_password, editText_confirm_password;
    private Button button_resetPassword;
    private ConstraintLayout constraintLayout_root;

    private String snackBarMessage = "";
    private String email = "";

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password3);

        context = this;

        email = getIntent().getStringExtra("email");

        constraintLayout_root = (ConstraintLayout) findViewById(R.id.activity_forgotpassword3_constraintLayout);
        editText_password = (EditText) findViewById(R.id.activity_forgotpassword3_edittext_password);
        editText_confirm_password = (EditText) findViewById(R.id.activity_forgotpassword3_edittext_confirm_password);
        button_resetPassword = (Button)findViewById(R.id.activity_forgotpassword3_button_reset_password);

        button_resetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonClickResetPassword();
            }
        });

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void buttonClickResetPassword(){
        if(checkNetworkStatus(context)){
            if(isValidator()){
                resetPassword();
            }
            else{
                showInvalidSnackBar(context, constraintLayout_root, snackBarMessage);
            }
        }
        else{
            showRedSnackBar(context, constraintLayout_root, "Please check your internet connection.");
        }
    }

    private void resetPassword(){

        showLoading(context, "Updating password...");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        UpdatePasswordRequestBody updatePasswordRequestBody = new UpdatePasswordRequestBody(email, editText_password.getText().toString());

        Call<UpdatePasswordResponseBody> call = apiService.getUpdatePasswordReponse(updatePasswordRequestBody);
        call.enqueue(new Callback<UpdatePasswordResponseBody>() {
            @Override
            public void onResponse(Call<UpdatePasswordResponseBody> call, Response<UpdatePasswordResponseBody> response) {
                stopLoading();
                createAccountResultAction(response.body());
            }

            @Override
            public void onFailure(Call<UpdatePasswordResponseBody> call, Throwable t) {
                stopLoading();
                showInvalidSnackBar(context, constraintLayout_root, "Oops! Something went wrong.");
            }
        });
    }

    private void createAccountResultAction(UpdatePasswordResponseBody body){
        if (!body.status){
            showInvalidSnackBar(context, constraintLayout_root, "Oops! Something went wrong..");
        }
        else{
            Intent i = new Intent(getBaseContext(), LoginActivity.class);
            i.putExtra("message", "passwordResetSuccessfully");
            startActivity(i);
        }
    }

    private boolean isValidator(){

        String password = editText_password.getText().toString();
        if(isNullOrEmptyOrWhitespace(password)){
            snackBarMessage = "Enter a valid password.";
            return false;
        }

        String confirmPassword = editText_confirm_password.getText().toString();
        if(isNullOrEmptyOrWhitespace(confirmPassword)){
            snackBarMessage = "Enter a valid confirm password.";
            return false;
        }

        if(!password.equals(confirmPassword)){
            snackBarMessage = "Confirm password is not equal to password";
            return false;
        }

        return true;
    }

}
