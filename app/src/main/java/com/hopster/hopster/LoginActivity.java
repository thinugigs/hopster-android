package com.hopster.hopster;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hopster.hopster.rest.ApiClient;
import com.hopster.hopster.rest.ApiInterface;
import com.hopster.hopster.rest.requestBodyObjects.LoginRequestBody;
import com.hopster.hopster.rest.responseBodyObjects.login.LoginResponseBody;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.hopster.hopster.commonComponents.Dialogs.showLoading;
import static com.hopster.hopster.commonComponents.Dialogs.stopLoading;
import static com.hopster.hopster.commonComponents.SnackBars.showSuccessSnackBar;
import static com.hopster.hopster.permissionCheck.commonPermissionCheck.checkNetworkStatus;
import static com.hopster.hopster.commonComponents.SnackBars.showInvalidSnackBar;
import static com.hopster.hopster.commonComponents.SnackBars.showRedSnackBar;
import static com.hopster.hopster.validations.CommonValidations.isNullOrEmptyOrWhitespace;
import static com.hopster.hopster.validations.CommonValidations.isValidEmail;

public class LoginActivity extends AppCompatActivity {

    private EditText editText_email, editText_password;
    private Button button_signin, button_create_account, button_forgot_password;
    private ConstraintLayout constraintLayout_root;

    private String snackBarMessage = "";

    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = this;

        constraintLayout_root = (ConstraintLayout) findViewById(R.id.activity_login_constraintLayout);
        editText_email = (EditText) findViewById(R.id.activity_login_edittext_email);
        editText_password = (EditText) findViewById(R.id.activity_login_edittext_password);
        button_create_account = (Button)findViewById(R.id.activity_login_button_create_account);
        button_forgot_password = (Button)findViewById(R.id.activity_login_button_forgot_password);
        button_signin = (Button)findViewById(R.id.activity_login_button_signin);

        button_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonClickSignIn();
            }
        });

        button_create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getBaseContext(), CreateAccountActivity.class);
                startActivity(i);
            }
        });

        button_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getBaseContext(), ForgotPassword1Activity.class);
                startActivity(i);
            }
        });

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if(getIntent().getExtras() != null){
            Bundle b = getIntent().getExtras();
            if(b.getString("message") != null){
                String message = b.getString("message");
                if(message.equals("accountCreatedSuccessfully")){
                    showSuccessSnackBar(context, constraintLayout_root, "Account created Successfully.");
                }
                else if(message.equals("passwordResetSuccessfully")){
                    showSuccessSnackBar(context, constraintLayout_root, "Reset password Successfully.");
                }
            }
        }


    }

    private void buttonClickSignIn(){
        if(checkNetworkStatus(context)){
            if(isValidator()){
                signIn();
            }
            else{
                showInvalidSnackBar(context, constraintLayout_root, snackBarMessage);
            }
        }
        else{
            showRedSnackBar(context, constraintLayout_root, "Please check your internet connection.");
        }
    }

    private void signIn(){

        showLoading(context, "Authenticating...");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        LoginRequestBody loginRequestBody = new LoginRequestBody(
                editText_email.getText().toString(),
                editText_password.getText().toString());

        Call<LoginResponseBody> call = apiService.getLoginReponse(loginRequestBody);
        call.enqueue(new Callback<LoginResponseBody>() {
            @Override
            public void onResponse(Call<LoginResponseBody> call, Response<LoginResponseBody> response) {
                stopLoading();
                signInResultAction(response.body());
            }

            @Override
            public void onFailure(Call<LoginResponseBody> call, Throwable t) {
                stopLoading();
                showInvalidSnackBar(context, constraintLayout_root, "Oops! Something went wrong.");
            }
        });
    }

    private void signInResultAction(LoginResponseBody body){
        if (!body.status){
            showInvalidSnackBar(context, constraintLayout_root, "Incorrect username or password");
        }
        else{
            Intent i = new Intent(getBaseContext(), HomeActivity.class);
            startActivity(i);
        }
    }

    private boolean isValidator(){

        String email = editText_email.getText().toString();
        if(isNullOrEmptyOrWhitespace(email) || !isValidEmail(email)){
            snackBarMessage = "Enter a valid email.";
            return false;
        }

        String password = editText_password.getText().toString();
        if(isNullOrEmptyOrWhitespace(password)){
            snackBarMessage = "Enter a valid password.";
            return false;
        }
        return true;
    }

}
